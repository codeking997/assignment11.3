﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGClasses;

namespace Andrius
{
    public partial class Form1 : Form
    {
        //in the constructor I have made it so that the background color of the panel is read
        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.Red;
            
            
        }
        /*
         * this method lets the user chose a thief,
         * there are only one thief to chose from however the logic in this method 
         * is the same as in all other methods
         * the if statement checks if the dropdown box if the user input equals stealth thief
         * if it does then it instantiates a stealth thief object
         * it then creates a streamwriter which creates a file 
         * the file then writes out the input given by the user for each field in the panel
         */
        private void ChoseThief()
        {
            if(comboBox1.SelectedItem.ToString()== "stealth thief")
            {
                
                StealthThief stealthThief = new StealthThief(txtName.Text, int.Parse(healthTxt.Text), int.Parse(manaTxt.Text), 
                    int.Parse(armorTxt.Text));
                
                StreamWriter file = new StreamWriter("test_file.txt");

                file.Write($"the name of your stealth thief is: {stealthThief.Name}\n");
                file.Write($"the health points of this stealth thief is: {stealthThief.HealthPoints}\n");
                file.Write($"the mana of this stealth thief is: {stealthThief.Mana}\n");
                file.Write($"the armor rating of this stealth thief is: {stealthThief.ArmorRating}\n");
                file.Close();
                

            }
        }
        /*
         * this method is similar to chose thief the only difference is that since there are
         * two differnt kinds of thiefs to chose from there is also an else if which basically 
         * does the same thing just that it gives the user the chance to create different types 
         * of wizards. 
         */
        private void ChoseWizard()
        {
            
            if (comboBox1.SelectedItem.ToString() == "water wizard")
            {
                
                WaterWizard waterWizard = new WaterWizard(txtName.Text, int.Parse(healthTxt.Text), int.Parse(manaTxt.Text),
                    int.Parse(armorTxt.Text));
                
                StreamWriter file = new StreamWriter("test_file.txt");

                file.Write($"the name of your water wizard is: {waterWizard.Name}\n");
                file.Write($"the health points of this water wizard is: {waterWizard.HealthPoints}\n");
                file.Write($"the mana of this water wizard is: {waterWizard.Mana}\n");
                file.Write($"the armor rating of this water wizard is: {waterWizard.ArmorRating}\n");
                file.Close();

            }else if (comboBox1.SelectedItem.ToString()== "fire wizard")
            {
                FireWizard fireWizard = new FireWizard(txtName.Text, int.Parse(healthTxt.Text), int.Parse(manaTxt.Text),
                    int.Parse(armorTxt.Text));
                
                StreamWriter file = new StreamWriter("test_file.txt");

                file.Write($"the name of your fire wizard is: {fireWizard.Name}\n");
                file.Write($"the health points of this fire wizard is: {fireWizard.HealthPoints}\n");
                file.Write($"the mana of this water fire is: {fireWizard.Mana}\n");
                file.Write($"the armor rating of this fire wizard is: {fireWizard.ArmorRating}\n");
                file.Close();
            }
        }
        /*
         * this method is identical to the chose wizard method
         * the difference is that it allows to chose between two different warriors
         * the logic is the same. 
         */
        private void ChoseWarrior()
        {
            
            if (comboBox1.SelectedItem.ToString()=="angry warrior")
            {
                AngryWarrior angryWarrior = new AngryWarrior(txtName.Text, int.Parse(healthTxt.Text), int.Parse(manaTxt.Text),
                    int.Parse(armorTxt.Text));
                
                
                
                
                StreamWriter file = new StreamWriter("test_file.txt");

                file.Write($"the name of your angry warrior is: {angryWarrior.Name}\n");
                file.Write($"the health points of this angry warrior is: {angryWarrior.HealthPoints}\n");
                file.Write($"the mana of this angry warrior is: {angryWarrior.Mana}\n");
                file.Write($"the armor rating of this angry warrior is: {angryWarrior.ArmorRating}\n");
                file.Close();

            }else if(comboBox1.SelectedItem.ToString()== "Armoured warrior")
            {
                ArmoredWarior armoredWarior = new ArmoredWarior(txtName.Text, int.Parse(healthTxt.Text), int.Parse(manaTxt.Text),
                    int.Parse(armorTxt.Text));
                StreamWriter file = new StreamWriter("test_file.txt");





                file.Write($"the name of your armoured warrior is: {armoredWarior.Name}\n");
                file.Write($"the health points of this armoured warrior is: {armoredWarior.HealthPoints}\n");
                file.Write($"the mana of this armoured warrior is: {armoredWarior.Mana}\n");
                file.Write($"the armor rating of this armoured warrior is: {armoredWarior.ArmorRating}\n");
                file.Close();
            }
        }
        
        /*
         * this method runs when the submit button is being clicked
         * when it is clicked it calls the methods defined above and runs them
         */
        private void button1_Click(object sender, EventArgs e)
        {
            
            ChoseWizard();
            ChoseWarrior();
            ChoseThief();
            

        }

        private void txtName_Click(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
